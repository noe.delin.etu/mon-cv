window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.querySelector("header").style.backgroundColor = "white";
    document.getElementById("returnToTheTop").style.display = "block";
  } else {
    document.querySelector("header").style.backgroundColor = "transparent";
    document.getElementById("returnToTheTop").style.display = "none";
  }
}

