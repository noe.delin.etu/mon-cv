let carousel = document.getElementById("carrousel");
let scrollWidth = carousel.scrollWidth;

function slideLeft() {
    if (carousel.scrollLeft === 0) {
        carousel.scrollTo({ left: scrollWidth, behavior: "auto"});
    } else {
        carousel.scrollBy({ left: -(scrollWidth/3), behavior: "auto"});
    }
    console.log(carousel.scrollLeft / carousel.scrollWidth * 100);
}

function slideRight() {
    if (carousel.scrollLeft >= scrollWidth * 2/3) {
        carousel.scrollTo({ left: 0, behavior: "auto"});
    } else {
        carousel.scrollBy({ left: scrollWidth/3, behavior: "auto"});
    }
    console.log(carousel.scrollLeft / carousel.scrollWidth * 100);
}

function getScrollPercent(element) {
    let h = element,
        b = carousel,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
}